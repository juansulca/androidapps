package com.jsulca.time_fighter


import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.navArgs
import com.evernote.android.state.StateSaver
import com.evernote.android.state.State
import com.google.firebase.firestore.FirebaseFirestore


class MainGame : Fragment() {

    internal lateinit var welcomeMessage: TextView
    internal lateinit var gameScoreTextView: TextView
    internal lateinit var tapButton: Button
    internal lateinit var timeLeftTextView: TextView

    val db = FirebaseFirestore.getInstance()
    var name = ""

    @State
    var score = 0

    private var initialCountDown: Long = 10000
    private var countDownInterval: Long = 1000
    private lateinit var countDownTimer: CountDownTimer

    @State
    var timeleft = 10

    @State
    var gameStarted = false

    val args: MainGameArgs by navArgs()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        StateSaver.restoreInstanceState(this, savedInstanceState) //option 1

        tapButton = view.findViewById(R.id.tap_button)
        welcomeMessage = view.findViewById(R.id.welcome_message)
        gameScoreTextView = view.findViewById(R.id.game_score)
        timeLeftTextView = view.findViewById(R.id.time_left)

        //option 2
        /*if(savedInstanceState != null){
            score = savedInstanceState.getInt(SCORE_KEY)
        }*/

        gameScoreTextView.text = getString(R.string.score_message, score)
        name = args.playerName.toString()
        welcomeMessage.text =getString(R.string.welcome_player, name)

        tapButton.setOnClickListener { incrementScore() }

        if(gameStarted) {
            restoreGame()
            return
        }

        resetGame()
    }


    private fun incrementScore(){
        if(!gameStarted){
            startGame()
        }
        score += 1
        gameScoreTextView.text = getString(R.string.score_message, score)
    }

    private fun resetGame(){
        score = 0
        gameScoreTextView.text = getString(R.string.score_message, score)

        timeleft = 10
        timeLeftTextView.text = getString(R.string.time_left_string, timeleft)

        countDownTimer = object: CountDownTimer(initialCountDown, countDownInterval){
            override fun onFinish() {

                endGame()

            }

            override fun onTick(millisUntilFinished: Long) {
                timeleft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text = getString(R.string.time_left_string, timeleft)
            }

        }
        gameStarted = false
        //tapButton.isClickable = true
    }

    private fun startGame(){

        countDownTimer.start()
        gameStarted = true

    }

    private fun endGame(){

        //tapButton.isClickable = false
        Toast.makeText(activity, getString(R.string.end_game, score), Toast.LENGTH_LONG).show()
        val data = HashMap<String, Any>()
        data["name"] = name
        data["score"] = score

        db.collection("players")
            .add(data)
            .addOnSuccessListener { documentReference ->
                Log.d("MAIN_GAME", "DocumentSnapshot added with ID: ${documentReference.id}")
            }
            .addOnFailureListener { e ->
                Log.w("MAIN_GAME", "Error adding document", e)
            }
        resetGame()

    }

    private fun restoreGame(){
        gameScoreTextView.text = getString(R.string.score_message, score)

        timeLeftTextView.text = getString(R.string.time_left_string, timeleft)

        countDownTimer = object: CountDownTimer(timeleft * 1000L, countDownInterval){
            override fun onFinish() {

                endGame()

            }

            override fun onTick(millisUntilFinished: Long) {
                timeleft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text = getString(R.string.time_left_string, timeleft)
            }

        }

        countDownTimer.start()
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        StateSaver.saveInstanceState(this, outState) //option 1
        countDownTimer.cancel()
        //outState.putInt(SCORE_KEY, score) //option 2
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_game, container, false)
    }

    //option 3
    /*companion object {
        private val SCORE_KEY = "SCORE"
    }*/

}
