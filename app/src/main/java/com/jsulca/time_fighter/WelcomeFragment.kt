package com.jsulca.time_fighter

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.navigation.findNavController


class WelcomeFragment : Fragment() {
    // TODO: Rename and change types of parameters

    private var listener: OnFragmentInteractionListener? = null
    internal lateinit var startButton: Button
    internal lateinit var playerName: TextView
    internal lateinit var scoresButton:Button


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        playerName = view.findViewById(R.id.player_name)
        startButton = view.findViewById(R.id.start_button)
        scoresButton = view.findViewById(R.id.scores_button)
        startButton.setOnClickListener { goToMainGame() }
        scoresButton.setOnClickListener { goToScores() }
    }

    fun goToMainGame(){
        val action =  WelcomeFragmentDirections.actionMainGame(playerName.text.toString())
        view?.findNavController()?.navigate(action)
    }

    fun goToScores(){
        val action =  WelcomeFragmentDirections.toWelcomeFragmentToScoresFragment()
        view?.findNavController()?.navigate(action)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_welcome, container, false)
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

}
